export class Constants {

    // Formulario 1 - Estado de Resultados
    // -----------------------------------------------------------------
    // INGRESOS DE VENTAS NETOS U OPERACIÓN
    public readonly FORM1_TOTAL_DE_INGRESOS = 'TOTAL DE INGRESOS';

    // Codigos
    // INGRESOS DE VENTAS NETOS U OPERACIÓN
    public readonly FORM1_COD_TOTAL_DE_INGRESOS = 'f1_tingreso';

}
